import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appFileDragAndDrop]'
})
export class FileDragAndDropDirective {
  @Output() fileDropped = new EventEmitter<File>();
  @Output() statusDragged = new EventEmitter<number>();

  @Input() sizeFile: number = 25;
  @HostListener('dragover', ['$event']) onDragOver(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    const transfer = event.dataTransfer;

    if(typeof transfer !== 'undefined' && !!transfer){
      const file = transfer.items[0];
      if (file.type.includes('image')) {
        event.dataTransfer.dropEffect = 'copy';
        this.statusDragged.emit(-1)
      } else {

        event.dataTransfer.dropEffect = 'none';
        this.statusDragged.emit(1)
      }
    }
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.statusDragged.emit(0);
  }

  @HostListener('drop', ['$event']) onDrop(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    const transfer = event.dataTransfer;

    if(typeof transfer !== 'undefined' && !!transfer){
      const file = transfer.files[0];

      const length = this.sizeFile * 1024 * 1024;

      if (length < file.size) {

        this.statusDragged.emit(2);

        setTimeout(() => {
          this.statusDragged.emit(0);
        }, 2000)
        return;
      }

      if (file) {
        this.statusDragged.emit(-2);
        setTimeout(() => {
          this.fileDropped.emit(file);
        }, 1000)
      }
    }

  }
  constructor() { }

}
