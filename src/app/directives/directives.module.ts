import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FileDragAndDropDirective} from "./file-drag-and-drop/file-drag-and-drop.directive";



@NgModule({
  declarations: [

    FileDragAndDropDirective
  ],
  exports: [
    FileDragAndDropDirective
  ],
  imports: [
    CommonModule
  ]
})
export class DirectivesModule { }
