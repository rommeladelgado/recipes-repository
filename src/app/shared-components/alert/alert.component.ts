import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {FormBuilder} from "@angular/forms";
import {INGREDIENTS, TYPES_PORTIONS} from "../../consts/recipe.consts";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnChanges {

  @Input() open: boolean;
  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() title: string;
  @Input() type: 'success' | 'danger' = 'success';
  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('template') template: TemplateRef<any>;
  modalRef?: BsModalRef | null;
  constructor(private modalService: BsModalService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('open' in changes && this.open) {
      this.openModal();
    }
  }

  openModal() {

    this.modalRef = this.modalService.show(this.template, {
      class: 'modal-dialog-centered modal-sm',
      backdrop : 'static',
      keyboard : false
    });
  }

  closeAlert() {
    this.modalRef?.hide()
    this.openChange.emit(false);
    if (this.type === 'danger') return;
    this.close.emit();
  }
}
