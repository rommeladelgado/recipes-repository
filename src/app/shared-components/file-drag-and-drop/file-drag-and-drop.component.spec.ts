import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileDragAndDrogComponent } from './file-drag-and-drog.component';

describe('FileDragAndDrogComponent', () => {
  let component: FileDragAndDrogComponent;
  let fixture: ComponentFixture<FileDragAndDrogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FileDragAndDrogComponent]
    });
    fixture = TestBed.createComponent(FileDragAndDrogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
