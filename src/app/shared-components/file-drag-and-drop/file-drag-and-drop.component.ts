import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';
import {DirectivesModule} from "../../directives/directives.module";
import {CommonModule, NgIf} from "@angular/common";

@Component({
  standalone: true,
  selector: 'app-file-drag-and-drop',
  templateUrl: './file-drag-and-drop.component.html',
  styleUrls: ['./file-drag-and-drop.component.scss'],
  imports: [
    DirectivesModule,
    CommonModule
  ]
})
export class FileDragAndDropComponent {

  status: number = 0;
  sizeFile: number = 25;
  @Output() onFile = new EventEmitter<File>();
  @ViewChild('inputFile') inputClickFile: ElementRef;
  onStatusDragged(mStatus: number) {
    this.status = mStatus;
  }

  setFile(mFile: File) {
    this.onFile.emit(mFile);
  }

  onSelectFile(event: Event) {

    const documentElement = event.currentTarget as HTMLInputElement;
    const fileList: FileList | null = documentElement.files;

    if (fileList) {
      const file = fileList[0];

      const length = this.sizeFile * 1024 * 1024;

      if (length < file.size) {

        this.status = 2;

        setTimeout(() => {
          this.status = 0;
        }, 2000)
        return;
      }

      if (file) {
        this.status = -2;
        setTimeout(() => {
          this.setFile(file);
        }, 1000)
      }
    }
  }

  onInputFile() {
    this.inputClickFile.nativeElement.click();
  }
}
