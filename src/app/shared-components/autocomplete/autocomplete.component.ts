import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {INGREDIENTS} from "../../consts/recipe.consts";
import {KeyTypesAutoComplete} from "../../models/ingredient.model";

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent {
  @Input() data: string[] = [];
  @Input() initValue: string = '';
  @Input() placeholder: string = '';
  @Input() label: string = '';
  @Input() value: string = '';
  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();


  get getInitValue(): string {
    if (this.initValue.length === 0 || typeof this.initValue === 'undefined' || this.data.length === 0 ) {
      return '';
    }

    const el = this.data.find((e) => e === this.initValue);

    if (typeof el === 'undefined') {
      return '';
    }
    return el;
  }


  selectEvent(item: string) {
   this.valueChange.emit(item);
  }

  onCleared(): void {
    this.valueChange.emit('');
  }
}
