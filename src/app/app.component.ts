import {Component, OnInit} from '@angular/core';
import {ServiceRecipes} from "./services/recipe.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'recipe-book';

  constructor(private serviceRecipes: ServiceRecipes) {
  }

  ngOnInit() {
    this.serviceRecipes.initState({
      recipes: [
        {
          id: '1',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '2',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '3',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '4',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '5',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '6',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '7',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '8',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '9',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '10',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '11',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '12',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '13',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '14',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '15',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '16',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '17',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '18',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
        {
          id: '19',
          imageUrl: 'https://s3.abcstatics.com/media/gurme/2022/07/14/s/recetas-pasta-faciles-rapidas-0003-kgLH--940x529@abc.jpg',
          title: 'Receta de Pastel de Chocolate',
          description: 'Un delicioso pastel de chocolate que hará que todos quieran más.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '1', name: 'Harina', extent: 'grams', quantity: 200 },
            { id: '2', name: 'Azúcar', extent: 'grams', quantity: 150 },
            { id: '3', name: 'Cacao en polvo', extent: 'grams', quantity: 50 },
            { id: '4', name: 'Leche', extent: 'ml', quantity: 200 },
            { id: '5', name: 'Huevos', extent: 'units', quantity: 2 },
          ]
        },
        {
          id: '20',
          imageUrl: 'https://img2.rtve.es/v/6538642?w=1600&preview=1652707560206.jpg',
          title: 'Ensalada de Pollo con Aguacate',
          description: 'Una ensalada refrescante y saludable con pollo a la parrilla y aguacate.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '6', name: 'Pechuga de pollo', extent: 'grams', quantity: 250 },
            { id: '7', name: 'Aguacate', extent: 'units', quantity: 1 },
            { id: '8', name: 'Lechuga', extent: 'grams', quantity: 100 },
            { id: '9', name: 'Tomate', extent: 'units', quantity: 2 },
            { id: '10', name: 'Aceite de oliva', extent: 'ml', quantity: 30 },
          ]
        },
        {
          id: '21',
          imageUrl: 'https://www.lacocinadelila.com/wp-content/uploads/2020/12/solomillo-wellington-600x450.jpg',
          title: 'Sopa de Verduras',
          description: 'Una sopa reconfortante llena de verduras frescas y sabor casero.',
          author: "Rommel Delgado",
          ingredients: [
            { id: '11', name: 'Zanahorias', extent: 'grams', quantity: 150 },
            { id: '12', name: 'Papas', extent: 'grams', quantity: 200 },
            { id: '13', name: 'Apio', extent: 'grams', quantity: 100 },
            { id: '14', name: 'Cebolla', extent: 'grams', quantity: 50 },
            { id: '15', name: 'Caldo de pollo', extent: 'ml', quantity: 500 },
          ]
        },
      ],
      activeId: '',
    });


  }
}
