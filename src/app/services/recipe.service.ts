import {BehaviorSubject, map, Observable} from "rxjs";

import {RecipeResumeModel, RecipeState} from "../models/recipe.model";
import {Injectable} from "@angular/core";
@Injectable({
  providedIn: 'root'
})
export class ServiceRecipes {
  observableRecipes: Observable<RecipeState>;
  public recipes$: BehaviorSubject<RecipeState>;

  constructor() {

  }

  public initState(initialState: RecipeState): void {
    this.recipes$ = new BehaviorSubject<RecipeState>(initialState);
    this.observableRecipes = this.recipes$.asObservable();
  }

  protected getState(): RecipeState {
    return this.recipes$.getValue()
  }
  protected setState(nextState: RecipeState): void {
    this.recipes$.next({ ...nextState });
  }

  public getStore(): Observable<RecipeState> {
    return this.observableRecipes;
  }

  public setId(item: RecipeResumeModel) {

    this.setState({
      ...this.getState(),
      activeId: item.id,
    })
  }

  public push(model: RecipeResumeModel): void {
    const values = this.getState().recipes;

    const filter = values.filter((e) => e.id === model.id);

    if (filter.length > 0) {
      const [element] = filter;
      const index = values.indexOf(element);
      values[index] = {...model};
      this.setState({
        ...this.getState(),
        recipes: [...values, model]
      });

    } else {
      this.setState({
        ...this.getState(),
        recipes: [...values, model]
      })
    }
  }

  public getRecipes(): Observable<RecipeResumeModel[]> {
    return this.getStore().pipe(map((e) => e.recipes.reverse() ));
  }

  public getActiveIndex(): string {
    return this.getState().activeId;
  }
  public getRecipeIndex(id: string): RecipeResumeModel | undefined {
    return this.getState().recipes.find((e) => e.id == id);
  }
}
