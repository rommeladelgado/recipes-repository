import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DetailComponent } from './components/detail/detail.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import {AppRoutingModule} from "./app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FileDragAndDropComponent} from "./shared-components/file-drag-and-drop/file-drag-and-drop.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ModalIngredientComponent } from './components/modal-ingredient/modal-ingredient.component';
import {ModalModule} from "ngx-bootstrap/modal";
import {TypeaheadModule} from "ngx-bootstrap/typeahead";
import {AutocompleteLibModule} from "angular-ng-autocomplete";
import { AutocompleteComponent } from './shared-components/autocomplete/autocomplete.component';
import { AlertComponent } from './shared-components/alert/alert.component';
import {ServiceRecipes} from "./services/recipe.service";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DetailComponent,
    RecipeComponent,
    ModalIngredientComponent,
    AutocompleteComponent,
    AlertComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FileDragAndDropComponent,
    ReactiveFormsModule,
    AutocompleteLibModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  providers: [ServiceRecipes],
  bootstrap: [AppComponent]
})
export class AppModule { }
