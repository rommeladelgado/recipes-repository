import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {IngredientResumeModel, KeyTypesAutoComplete} from "../../models/ingredient.model";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {INGREDIENTS, TYPES_PORTIONS} from "../../consts/recipe.consts";


@Component({
  selector: 'app-modal-ingredient',
  templateUrl: './modal-ingredient.component.html',
  styleUrls: ['./modal-ingredient.component.scss']
})
export class ModalIngredientComponent implements OnChanges, OnInit {


  ingredients: string[];
  typesPortions: string[];

  asyncSelected: string;
  modalRef?: BsModalRef | null;
  @Input() ingredient: IngredientResumeModel;
  @Output() save: EventEmitter<IngredientResumeModel> = new EventEmitter<IngredientResumeModel>();
  @Input() open: boolean;
  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('template') template: TemplateRef<any>;

  errorId: 'name' | 'quantity' | 'extent' | null = null;
  constructor(private modalService: BsModalService,private formBuilder: FormBuilder) {}

  ngOnInit(): void {

    this.ingredients = [...INGREDIENTS];
    this.typesPortions = [...TYPES_PORTIONS];
  }



  ngOnChanges(changes: SimpleChanges): void {
    if ('open' in changes && this.open) {
      this.openModal();
    }
  }

  openModal() {

    this.modalRef = this.modalService.show(this.template, {
      class: 'modal-dialog-centered modal-lg',
      backdrop : 'static',
      keyboard : false
    });
  }

  closeModal() {
    this.modalRef?.hide()
    this.openChange.emit(false);
  }

  saveIngredient() {


    if (this.ingredient.name.length > 0 && this.ingredient.extent.length > 0 && this.ingredient.quantity > 0) {
      this.save.emit({...this.ingredient});
      this.closeModal();
    } else if (this.ingredient.name.length === 0) {
      this.errorId = 'name';
    } else if (this.ingredient.extent.length === 0) {
      this.errorId = 'extent';
    } else if (this.ingredient.quantity === 0) {
      this.errorId = 'quantity';
    }

    setTimeout(() => {
      this.errorId = null;
    }, 3000);
  }



}
