import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {INGREDIENTS, TYPES_PORTIONS} from "../../consts/recipe.consts";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {IngredientModel, IngredientResumeModel} from "../../models/ingredient.model";
import {RecipeResumeModel} from "../../models/recipe.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ServiceRecipes} from "../../services/recipe.service";

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements  OnInit{
  ingredients: IngredientResumeModel [] = [];
  //typesPortions: string [] = [];
  file: File | null = null;
  imageUrl: string;
  formRecipe: FormGroup;

  ingredient: IngredientResumeModel;
  openIngredientModal: boolean = false;
  openAlert: boolean = false;
  typeAlert: 'success' | 'danger' = 'success';
  titleAlert: string = '';

  errorId: 'title' | 'description' | 'author' | null;
  constructor(private formBuilder: FormBuilder, private router: Router, private service: ServiceRecipes, private route: ActivatedRoute) {

  }
  ngOnInit(): void {
    this.initForm();


    this.ingredient = this.resetIngredient();

    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        const val = this.service.getRecipeIndex(id);

        if (val) {
          this.imageUrl = val.imageUrl;
          this.ingredients = val.ingredients;
          this.formRecipe.setValue({
            title: val.title,
            description: val.description,
            author: val.author,
          });
        }

        this.service.setId({id} as RecipeResumeModel);

      }

    });


  }

  initForm() {

    this.formRecipe =  this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required],
    });

  }

  isFieldValid(field: 'title' | 'description' | 'author') {
    return !this.formRecipe.get(field)?.valid && this.formRecipe.get(field)?.touched;
  }

  onFile(mFile: File) {
    this.file = mFile;
    this.imageUrl = URL.createObjectURL(mFile);
  }

  onAddIngredient() {
    this.openIngredientModal = true;
  }


  resetIngredient(): IngredientResumeModel {
    const id = this.generateID();

    return {
      id,
      name: '',
      extent: '',
      quantity: 0
    } as IngredientResumeModel
  }
  generateID(): string {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let id = '';
    for (let i = 0; i < 8; i++) {
      const index = Math.floor(Math.random() * characters.length);
      id += characters.charAt(index);
    }
    return id;
  }

  onSave(event: IngredientResumeModel) {

    const arr = this.ingredients.filter((e) => e.id === event.id);
    if (arr.length === 0) {
      this.ingredients.push({...event});
    } else {
      const [first] = arr;
      const i = this.ingredients.indexOf(first);
      this.ingredients[i] = {...event};
    }

    this.ingredient = this.resetIngredient();
  }

  openEditIngredient(ingredientResumeModel: IngredientResumeModel) {
    this.ingredient = { ...ingredientResumeModel};
    this.openIngredientModal = true;
  }

  removeIngredient(ingredientResumeModel:IngredientResumeModel) {
    const i = this.ingredients.indexOf(ingredientResumeModel);

    this.ingredients = [...this.ingredients.filter((_, index) => index !== i)];

  }

  validateFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {

      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      }
    });
  }
  saveRecipe() {



    if (this.formRecipe.valid) {
      const form: RecipeResumeModel = this.formRecipe.value;

      if (this.imageUrl === null) {
        this.typeAlert = 'danger';
        this.titleAlert = 'Aun no se ha agregado una imagen de la receta';
      } else if (this.ingredients.length === 0) {
        this.typeAlert = 'danger';
        this.titleAlert = 'Aun no se ha agregado ingredientes';
      } else {
        this.typeAlert = 'success';
        this.titleAlert = 'Guardado con éxito!!';

        const index  = this.service.getActiveIndex();
        let id;
        if (index.length > 0) {
          id = index;
        } else {
          id = this.generateID();
        }

        this.service.push({
          imageUrl: this.imageUrl,
          description: form.description,
          title: form.title,
          author: form.author,
          ingredients: this.ingredients,
          id,
        });
        this.service.setId({ id: ''} as RecipeResumeModel);
      }

      this.openAlert = true;
    } else {
      this.validateFields(this.formRecipe);
    }
  }

  onCloseAlertAction() {
    this.router.navigate(['']);
  }
}
