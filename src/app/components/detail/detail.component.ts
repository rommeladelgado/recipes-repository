import {Component, OnInit} from '@angular/core';
import {RecipeModel, RecipeResumeModel} from "../../models/recipe.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ServiceRecipes} from "../../services/recipe.service";
import {IngredientModel, IngredientResumeModel} from "../../models/ingredient.model";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit{
  recipe: RecipeModel = { } as RecipeModel;

  constructor(private router: Router, private service: ServiceRecipes, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        const val = this.service.getRecipeIndex(id);

        if (val) {
          this.recipe = {
            ...val,
            ingredients: val.ingredients.map((e) =>({...e, status: false} as IngredientModel))
          }
        }

        this.service.setId({id} as RecipeResumeModel);

      }

    });
  }

  onClose() {
    this.router.navigate(['']);
  }

  onCheckIngredient(item: IngredientModel) {
    const values = this.recipe.ingredients;

    const index = values.findIndex((e) => item.id === e.id);
    values[index] = {...values[index], status: !values[index].status};
    this.recipe.ingredients = [...values];
  }

  get getReadyPrepare() {
    return this.recipe.ingredients.filter((e) => e.status).length === this.recipe.ingredients.length;
  }
}
