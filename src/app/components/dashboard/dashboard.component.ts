import {Component, OnInit} from '@angular/core';
import {ServiceRecipes} from "../../services/recipe.service";
import {RecipeResumeModel} from "../../models/recipe.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{


  recipes: RecipeResumeModel[];
  constructor(private serviceRecipes: ServiceRecipes, private router: Router,) {
  }

  ngOnInit() {

    this.serviceRecipes.getRecipes().subscribe((res) => {
      this.recipes = res;
    });
  }


  editRecipe(item: RecipeResumeModel) {
    this.serviceRecipes.setId(item);
    this.router.navigate([`recipe/${item.id}`]);
  }

  prepareRecipe(recipe: RecipeResumeModel) {
    this.serviceRecipes.setId(recipe);
    this.router.navigate([`detail/${recipe.id}`]);
  }
}
