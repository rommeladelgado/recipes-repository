import {IngredientModel, IngredientResumeModel} from "./ingredient.model";

export interface RecipeModel {
  id: string,
  imageUrl: string,
  title: string,
  description: string,

  author: string,
  ingredients: IngredientModel[]
}

export type RecipeResumeModel = Omit<RecipeModel, 'ingredients'> & {
  ingredients: IngredientResumeModel[]
}

export interface RecipeState {
  recipes: RecipeResumeModel[],
  activeId: string,
}
