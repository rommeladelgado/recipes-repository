export interface IngredientModel {
  id: string,
  name: string,
  extent: string,
  quantity: number,
  status: boolean,
}

export type IngredientResumeModel = Omit<IngredientModel,'status'>


export type KeyTypesAutoComplete = { id: number, name: string };
